export const getRGB = (H, S, L) => {
  let R, G, B = null;

  if (S === 0){
    R = G = B = L;
  } else {
    const hue2rgb = (P, Q, T) => {
      if (T < 0) {
        T += 1;
      }

      if (T > 1) {
        T -= 1;
      }

      if (T < 1/6) {
        return P + (Q - P) * 6 * T;
      }

      if (T < 1/2) {
        return Q;
      }

      if (T < 2/3) {
        return P + (Q - P) * (2 / 3 - T) * 6;
      }

      return P;
    }

    const Q = L < 0.5 ? L * (1 + S) : L + S - L * S;
    const P = 2 * L - Q;

    R = hue2rgb(P, Q, H + 1 / 3);
    G = hue2rgb(P, Q, H);
    B = hue2rgb(P, Q, H - 1 / 3);
  }

  return [Math.round(R * 255), Math.round(G * 255), Math.round(B * 255)];
};

export const getHSL = (R, G, B) => {
  R /= 255;
  G /= 255;
  B /= 255;

  const max = Math.max(R, G, B);
  const min = Math.min(R, G, B);
  let H, S, L = (max + min) / 2;

  if (max === min){
    H = S = 0;
  } else {
    const D = max - min;

    S = L > 0.5 ? D / (2 - max - min) : D / (max + min);

    switch (max) {
      case R:
        H = (G - B) / D + (G < B ? 6 : 0);
        break;

      case G:
        H = (B - R) / D + 2;
        break;

      case B:
        H = (R - G) / D + 4;
        break;

      default:
        break;
    }

    H /= 6;
  }

  return [H, S, L];
};