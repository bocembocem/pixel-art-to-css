export const getBoxShadow = (layer, pixelSize) => {
	let boxShadow = '';

	for (let y in layer) {
		for (let x in layer[y]) {
			const [R, G, B, A] = layer[y][x];
			const width = (Number(x) + 1) * pixelSize;
			const height = (Number(y) + 1) * pixelSize;

			boxShadow += `${width}px ${height}px 0 0 rgba(${R}, ${G}, ${B}, ${A}), `;
		}
	}
	
	if (!boxShadow) {
		return 'none';
	}

	boxShadow = boxShadow.slice(0, -2);

	return boxShadow;
};
