import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import styles from './UserPage.module.css';

import UserBar from '../components/UserBar/UserBar';
import ProjectList from '../components/ProjectList/ProjectList';
import { loadProject, changeProjectIdx } from '../redux/reducers/project/actions';
import apiWithInfoModal from '../services/apiWithInfoModal';

const UserPage = props => {
  const {
    token,
    name,
    id,
    activeProjectIdx,
    loadProject,
    changeProjectIdx
  } = props;
  const [tabShared, setTabShared] = useState(false);
  const [projects, setProjects] = useState(null);

  const deleteHandler = idx => {
    apiWithInfoModal.deleteProject(token, idx)
      .then(r => {
        if (r.status) {
          setProjects(null);
          setProjects(r.projects);
          
          if (activeProjectIdx) {
            if (idx === activeProjectIdx) {
              changeProjectIdx(null);
            } else {
              changeProjectIdx(activeProjectIdx - 1);
            }
          }
        }
      });
  };
  const editorHandler = idx => loadProject(idx, projects[idx]);
  const shareHandler = idx =>
    apiWithInfoModal.shareProject({ token, name, id, project: projects[idx]});

  useEffect(() => {
    if (token) {
      const response = tabShared
        ? apiWithInfoModal.loadUserSharedProjects(token)
        : apiWithInfoModal.loadProjects(token);

      response.then(r => r.status && setProjects(r.projects));
    }    
  }, [tabShared, token]);

  
  if (!token) {
    return (
      <div className={styles.user}>
        <span>Need auth</span>
      </div>
    );
  }

  return (
    <div className={styles.user}>
      <UserBar
        name={name}
        tabShared={tabShared}
        setTabShared={bool => {
          setProjects(null);
          setTabShared(bool);
        }}
      />
      <ProjectList
        projectList={projects}
        editorHandler={editorHandler}
        deleteHandler={!tabShared ? deleteHandler : null}        
        shareHandler={!tabShared ? shareHandler : null}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  token: state.user.token,
  name: state.user.name,
  id: state.user.id,
  activeProjectIdx: state.project.projectIdx
});
const mapDispatchToProps = { loadProject, changeProjectIdx };

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);