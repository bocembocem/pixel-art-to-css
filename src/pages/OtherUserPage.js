import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import styles from './OtherUserPage.module.css';

import ProjectList from '../components/ProjectList/ProjectList';
import { loadProject } from '../redux/reducers/project/actions';
import apiWithInfoModal from '../services/apiWithInfoModal';

const OtherUserPage = ({ loadProject, match, history }) => {
  const id = Number(match.params.id);
  const [name, setName] = useState(null);
  const [projects, setProjects] = useState(null);

  const editorHandler = idx => loadProject(null, projects[idx]);

  useEffect(() => {
    apiWithInfoModal.getUserCount()
      .then(r => {
        if (r.status) {
          if (r.current > id) {
            apiWithInfoModal.getUserInfo(id)
              .then(r => {
                if (r.status) {
                  setName(r.name);
                  setProjects(r.projects);
                }
              });
          } else {
            history.push('/shared/1');
          }
        }
      });
  }, []);

  return (
    <div className={styles.user}>
      <div className={styles.title}>
        User id: {id}{name ? `, Name: ${name}` : null}
      </div>
      <ProjectList projectList={projects} editorHandler={editorHandler} />
    </div>
  );
};

const mapDispatchToProps = { loadProject };

export default connect(null, mapDispatchToProps)(OtherUserPage);