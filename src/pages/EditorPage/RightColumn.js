import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowsAltH, faArrowsAltV } from '@fortawesome/free-solid-svg-icons';

import styles from './RightColumn.module.css';

import Button from '../../components/Button/Button';
import SizeCounter from '../../components/SizeCounter/SizeCounter';
import SizeInput from '../../components/SizeInput/SizeInput';
import {
  resetLayerColorMap,
  changeGridSize,
  changePixelSize,
  changeDuration
} from '../../redux/reducers/project/actions';
import { togglePreviewModal } from '../../redux/reducers/modals/actions';

const RightColumn = props => {
	const {
		params,
		togglePreviewModal,
		resetLayerColorMap,
		changeGridSize,
		changePixelSize,
		changeDuration
	} = props;

	return (		
    <div className={styles.right}>
      <div className={styles.button}>
        <Button
          value={'Preview'}
          clickHandler={togglePreviewModal}
        />
      </div>
      <div className={styles.button}>
        <Button
          value={'Reset'}
          clickHandler={resetLayerColorMap}
        />
      </div>
      <div className={styles.counter}>
        <span>
          <FontAwesomeIcon size="2x" icon={faArrowsAltH} />
        </span>
        <span>
          <SizeCounter
            size={params.gridX}
            changeHandler={newSize => 
              changeGridSize({ gridX: newSize })}
          />
        </span>
      </div>
      <div className={styles.counter}>
        <span>
          <FontAwesomeIcon size="2x" icon={faArrowsAltV} />
        </span>
        <span>
          <SizeCounter
            size={params.gridY}
            changeHandler={newSize => 
              changeGridSize({ gridY: newSize })}
          />
        </span>
      </div>
      <div className={styles.input}>
        <SizeInput
          title={'Pixel size'}
          value={params.pixelSize}
          changeHandler={changePixelSize}
        />
      </div>
      <div className={styles.input}>
        <SizeInput
          title={'Duration'}
          value={params.duration}
          changeHandler={changeDuration}
        />
      </div>
    </div>
	);
};

const mapStateToProps = state => ({ params: state.project.params });
const mapDispatchToProps = {
	togglePreviewModal,
	resetLayerColorMap,
  changeGridSize,
  changePixelSize,
	changeDuration
};

export default connect(mapStateToProps, mapDispatchToProps)(RightColumn);