import React from 'react';
import { connect } from 'react-redux';

import styles from './LeftColumn.module.css';

import Button from '../../components/Button/Button';
import Palette from '../../components/Palette/Palette';
import Tools from '../../components/Tools/Tools';
import ColorPicker from '../../components/ColorPicker/ColorPicker';
import {
  addProject,
  changeProjectIdx,
  onHistoryPrev,
  onHistoryNext
} from '../../redux/reducers/project/actions';
import { toggleLoadModal, toggleInfoModal } from '../../redux/reducers/modals/actions';
import apiWithInfoModal from '../../services/apiWithInfoModal';

const emptyCheck = layers => {
  for (let i = 0; i < layers.length; i++) {
    for (let x in layers[i]) {
      for (let y in layers[i][x]) {
        if (layers[i][x][y]) {
          return true;
        }
      }
    }
  }

  return false;
};

const LeftColumn = props => {
	const {
		token,
		projectIdx,
		params,
		colorPicker,
		addProject,
		changeProjectIdx,
		onHistoryPrev,
		onHistoryNext,
		toggleLoadModal,
		toggleInfoModal
	} = props;

	const saveProjectHandler = () => {
		if (!token) {
			toggleInfoModal(true, 'Need auth', true);
		} else if (emptyCheck(params.layerList)) {
      apiWithInfoModal.saveProject(token, params, projectIdx)
        .then(r => r.status && changeProjectIdx(r.idx));
    } else {
      toggleInfoModal(true, 'Empty project', true);
    }
  };

	return (    
    <div className={styles.left}>
      <div className={styles.button}>
        <Button value={'New'} clickHandler={addProject} />
      </div>
      <div className={styles.buttons}>
        <div className={styles.button}>
          <Button value={'Load'} clickHandler={toggleLoadModal} />
        </div>
        <div className={styles.button}>
          <Button value={'Save'} clickHandler={saveProjectHandler} />
        </div>
      </div>
      <div className={styles.buttons}>
        <div className={styles.button}>
          <Button value={'Prev'} clickHandler={onHistoryPrev} />
        </div>
        <div className={styles.button}>
          <Button value={'Next'} clickHandler={onHistoryNext} />
        </div>
      </div>
      <Palette />
      <Tools />
      {colorPicker ? <ColorPicker /> : null}
    </div>
	);
};

const mapStateToProps = state => ({
	token: state.user.token,
	projectIdx: state.project.projectIdx,
	params: state.project.params,
	colorPicker: state.tools.colorPicker
});
const mapDispatchToProps = {
	addProject,
	changeProjectIdx,
	toggleLoadModal,
  onHistoryPrev,
  onHistoryNext,
	toggleInfoModal
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftColumn);