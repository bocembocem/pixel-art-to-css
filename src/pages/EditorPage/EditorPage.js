import React from 'react';

import styles from './EditorPage.module.css';

import Layers from '../../components/Layers/Layers';
import LeftColumn from './LeftColumn';
import RightColumn from './RightColumn';
import Ground from '../../components/Ground/Ground';

export default () => (
	<div className={styles.editor}>
		<div className={styles.layers}>
			<Layers />
		</div>
		<div className={styles.columns}>
			<div className={styles.left}>
				<LeftColumn />
			</div>			
			<div className={styles.center}>
				<Ground />
			</div>
			<div className={styles.right}>
				<RightColumn />
			</div>			
		</div>
	</div>
);