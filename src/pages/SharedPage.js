import React, { useState, useEffect }  from 'react';
import { connect } from 'react-redux';

import styles from './SharedPage.module.css';

import ProjectList from '../components/ProjectList/ProjectList';
import SharedPageNav from '../components/SharedPageNav/SharedPageNav';
import { loadProject } from '../redux/reducers/project/actions';
import apiWithInfoModal from '../services/apiWithInfoModal';

const SharedPage = ({ match, history, loadProject }) => {
  const id = Number(match.params.id);
  const [pagesCount, setPagesCount] = useState(null);
  const [projects, setProjects] = useState(null);

  const editorHandler = idx => loadProject(null, projects[idx].project);

  useEffect(() => {
    apiWithInfoModal.getSharedProjectsCount()
      .then(r => r.status && setPagesCount(r.count));
  }, []);
  useEffect(() => {
    if (pagesCount) {
      if (id > pagesCount) {
        history.push('/shared/1');
      } else {
        setProjects(null);

        apiWithInfoModal.loadSharedProjects(id)
          .then(r => r.status && setProjects(r.projects));
      }
    }
  }, [id, pagesCount]);


  return (
    <div className={styles.shared}>      
      <div className={styles.title}>Shared projects</div>
      <ProjectList projectList={projects} editorHandler={editorHandler} />
      <SharedPageNav id={id} pagesCount={pagesCount} />
    </div>
  );
};

const mapDispatchToProps = { loadProject };

export default connect(null, mapDispatchToProps)(SharedPage)