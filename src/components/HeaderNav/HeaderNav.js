import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import styles from './HeaderNav.module.css';

import Button from '../Button/Button';
import { changeProjectIdx } from '../../redux/reducers/project/actions';
import { toggleAuthModal } from '../../redux/reducers/modals/actions';
import { onSignOut } from '../../redux/reducers/user/actions';
import apiWithInfoModal from '../../services/apiWithInfoModal';

const HeaderNav = ({ name, changeProjectIdx, toggleAuthModal, onSignOut }) => {
  const signUpHandler = () => toggleAuthModal(true, true);
	const signInHandler = () => toggleAuthModal(true, false);
  const signOutHandler = () =>
    apiWithInfoModal.signOut()
      .then(() => {
        onSignOut();
        changeProjectIdx(null);
      });

  const getUserButton = () => {
    if (!name) {
      return null;
    }

    return (
      <NavLink to='/user'>
        <div className={styles.button}>
          <Button value={name} />
        </div>
      </NavLink>
    );
  };
  const getSignButtons = () => {
    if (name) {
      return (
        <div className={styles.button}>
					<Button value='Sign out' clickHandler={signOutHandler} />
				</div>
      );
    }

    return (
      <Fragment>
        <div className={styles.button}>
          <Button value='Sign up' clickHandler={signUpHandler} />
        </div>
        <div className={styles.button}>
          <Button value='Sign in' clickHandler={signInHandler} />
        </div>
      </Fragment>
    );
  };

  return (
    <div className={styles.nav}>
      {getUserButton()}
      <NavLink to='/'>
        <div className={styles.button}>
          <Button value={'Editor'} />
        </div>
      </NavLink>
      <NavLink to='/shared/1'>
        <div className={styles.button}>
          <Button value='Shared' />
        </div>
      </NavLink>
      {getSignButtons()}
    </div>
  );
};

const mapStateToProps = state => ({ name: state.user.name });
const mapDispatchToProps = {
  changeProjectIdx,
  toggleAuthModal,
  onSignOut,
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderNav);