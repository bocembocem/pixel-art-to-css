import React from 'react';

import styles from './Canvas.module.css';

import CanvasLine from '../CanvasLine/CanvasLine';

export default ({ gridX, gridY, map }) => {
	const lines = [];

	for (let y = 0; y < gridY; y++) {
		lines[y] = <CanvasLine gridX={gridX} mapY={map[y] || null} key={y} />;
	}

	return <div className={styles.canvas}>{lines}</div>;
};