import React from 'react';

import styles from './SizeCounter.module.css';

export default ({ size, changeHandler }) => (
	<div className={styles.counter}>
		<input
			onChange={e => changeHandler(parseInt(e.target.value))}
			type="text"
			className={styles.input}
			value={size}
		/>
		<div className={styles.controls}>
			<span className={styles.button} onClick={() => changeHandler(size + 1)}>
				+
			</span>
			<span className={styles.button} onClick={() => changeHandler(size - 1)}>
				-
			</span>
		</div>
	</div>
);
