import React from 'react';

import styles from './ListenerLayer.module.css';

export default ({ onStart, onMove, onEnd }) => (
  <div
    className={styles.layer}				
    onMouseDown={onStart}
    onMouseMove={onMove}
    onMouseUp={onEnd}
    onMouseLeave={onEnd}
    onDragStart={e => e.preventDefault()}
  />
);