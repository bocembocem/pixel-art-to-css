import React from 'react';
import { connect } from 'react-redux';
import cn from 'classnames';

import styles from './Palette.module.css';

import { changeActiveColorIdx } from '../../redux/reducers/colors/actions';

const Palette = ({ activeColorIdx, colorList, changeActiveColorIdx }) => {
	const colors = colorList.map((color, idx) => {
		const [R, G, B, A] = color;

		return (
			<div
				className={
					activeColorIdx === idx
						? cn(styles.item, styles.active)
						: styles.item
				}
				style={{ backgroundColor: `rgba(${R},${G},${B},${A})` }}
				onClick={() => changeActiveColorIdx(idx)}
				key={idx}
			/>
		);
	});

	return (
		<div className={styles.palette}>
			{colors}
		</div>
	);
};

const mapStateToProps = state => ({
	activeColorIdx: state.colors.activeColorIdx,
	colorList: state.colors.colorList
});

const mapDispatchToProps = { changeActiveColorIdx };

export default connect(mapStateToProps, mapDispatchToProps)(Palette);
