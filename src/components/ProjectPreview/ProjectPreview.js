import React, { useRef, useEffect } from 'react';

import styles from './ProjectPreview.module.css';

import { getBoxShadow } from '../../helpers/getBoxShadow';

export default props => {
  const {
    activeLayerIdx,
    layerList,
    gridX,
    gridY,
    pixelSize,
    duration,
    animation
  } = props;
  const previewRef = useRef(null);
  const boxShadow = animation
    ? layerList.map(layer => getBoxShadow(layer, pixelSize))
    : [getBoxShadow(layerList[activeLayerIdx], pixelSize)];
  
  useEffect(() => {
		if (animation && boxShadow.length > 1) {
			let count = 1;
			const showNextLayer = () => {
				previewRef.current.style.boxShadow = boxShadow[count];
				count = count + 1 === boxShadow.length ? 0 : count + 1;
			};
			const interval = setInterval(showNextLayer, duration * 1000 / layerList.length);

			return () => clearInterval(interval);
		}
	}, [animation]);

  return (
    <div
      className={styles.preview}
      style={{
        width: `${gridX * pixelSize}px`,
        height: `${gridY * pixelSize}px`
      }}
    >
      <span
        ref={previewRef}
        style={{
          boxShadow: boxShadow[0],
          top: -pixelSize,
          left: -pixelSize,
          width: `${pixelSize}px`,
          height: `${pixelSize}px`
        }}
      />
    </div>
  );
};