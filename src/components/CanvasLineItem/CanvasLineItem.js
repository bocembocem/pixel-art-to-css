import React from 'react';

import styles from './CanvasLineItem.module.css';

export default ({ gridX, color }) => {
	const style = {
		width: `${100 / gridX}%`,
		paddingBottom: `${100 / gridX}%`,
		backgroundColor: 'rgba(0,0,0,0)'
	};

	if (color) {
		const [R, G, B, A] = color;

		style.backgroundColor = `rgba(${R},${G},${B},${A})`;
	}							

	return <div className={styles.item} style={style} />;
};