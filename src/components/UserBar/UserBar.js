import React from 'react';

import styles from './UserBar.module.css';

import TogglerButton from '../TogglerButton/TogglerButton';

export default ({ name, tabShared, setTabShared }) => (
  <div className={styles.userbar}>
    <div className={styles.togglers}>
      <div className={styles.toggler}>
        <TogglerButton 
          value={'My projects'}
          clickHanler={() => setTabShared(false)}
          active={!tabShared}
        />
      </div>
      <div className={styles.toggler}>
        <TogglerButton 
          value={'My shared projects'}
          clickHanler={() => setTabShared(true)}
          active={tabShared}
        />
      </div>
    </div>      
    <div className={styles.name}>
      Hello {name}!
    </div>
  </div>
);