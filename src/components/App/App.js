import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import styles from './App.module.css';

import Header from '../Header/Header';
import EditorPage from '../../pages/EditorPage/EditorPage';
import UserPage from '../../pages/UserPage';
import SharedPage from '../../pages/SharedPage';
import OtherUserPage from '../../pages/OtherUserPage';
import AuthModal from '../AuthModal/AuthModal';
import InfoModal from '../InfoModal/InfoModal';
import LoadModal from '../LoadModal/LoadModal';
import PreviewModal from '../PreviewModal/PreviewModal';
import CustomBG from '../CustomBG/CustomBG';

const App = ({ activeLayer, pixelSize, modals }) => (
	<Router>
		<CustomBG map={activeLayer} pixelSize={pixelSize} />
		<div className={styles.app}>
			<div className={styles.container}>
				<Header />
				<Switch>
					<Route path='/' component={EditorPage} exact />
					<Route path='/user' component={UserPage} exact />
					<Route path='/user/:id' component={OtherUserPage} exact />
					<Route path='/shared/:id' component={SharedPage} exact />
				</Switch>				
				{modals.auth.visible ? <AuthModal /> : null}	
				{modals.info.visible ? <InfoModal /> : null}				
				{modals.load ? <LoadModal /> : null}
				{modals.preview ? <PreviewModal /> : null}
			</div>			
		</div>		
	</Router>
);

const mapStateToProps = state => {
  const { layerList, activeLayerIdx, pixelSize } = state.project.params;

  return {
		activeLayer: layerList[activeLayerIdx],
		pixelSize,
		modals: state.modals
	};
};

export default connect(mapStateToProps)(App);