import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';

import styles from './PreviewModal.module.css';

import TogglerButton from '../TogglerButton/TogglerButton';
import ProjectPreview from '../ProjectPreview/ProjectPreview';
import { togglePreviewModal } from '../../redux/reducers/modals/actions';
import { getBoxShadow } from '../../helpers/getBoxShadow';

const getKeyframes = (layerList, pixelSize, duration) => {
	const delta = parseInt(100 / layerList.length);
	const moments = layerList.map((layer, idx) => {
		const boxShadow = getBoxShadow(layer, pixelSize);
		const percent = idx === 0
			? `${idx}%, ${delta * (idx + 1)}% {`
			: `${delta * (idx) + 0.01}%, ${delta * (idx + 1)}% {`;

		return (
			<span key={idx}>
				<span>&nbsp;&nbsp;{percent}</span>
				<span>&nbsp;box-shadow: {boxShadow};</span>
				<span>&nbsp;}</span>
			</span>
		);
	});

	return (
		<Fragment>
			<span>{'.pixel-art {'}</span>
			<span>&nbsp;&nbsp;position: absolute;</span>
			<span>&nbsp;&nbsp;{`animation: x ${duration}s infinite;`}</span>
			<span>{'}'}</span>
			<br />

			<span>{'@keyframes x {'}</span>
			{moments}
			<span>}</span>
		</Fragment>
	);
};
const getSingle = (layer, pixelSize) => {
	const boxShadow = getBoxShadow(layer, pixelSize);

	return (
		<Fragment>
			<span>{'.pixel-art {'}</span>
			<span>&nbsp;&nbsp;{`box-shadow: ${boxShadow};`}</span>
			<span>&nbsp;&nbsp;{`width: ${pixelSize}px;`}</span>
			<span>&nbsp;&nbsp;{`height: ${pixelSize}px;`}</span>
			<span>{'}'}</span>
		</Fragment>
	);
};

const PreviewModal = ({ params, togglePreviewModal }) => {
	const {
		activeLayerIdx,
		layerList,
		pixelSize,
		duration
	} = params;
	const [single, setSingle] = useState(true);

	const code = single
		? getSingle(layerList[activeLayerIdx], pixelSize)
		: getKeyframes(layerList, pixelSize, duration);

	return (
		<div className={styles.modal}>
			<div className={styles.inner}>
				<div className={styles.togglers}>
					<div className={styles.toggler}>
						<TogglerButton
							value={'Single'}
							clickHanler={() => setSingle(true)}
							active={single}
						/>
					</div>
					<div className={styles.toggler}>
						<TogglerButton
							value={'Animation'}
							clickHanler={() => setSingle(false)}
							active={!single}
						/>
					</div>
				</div>
				<div className={styles.project}>
					<ProjectPreview {...params} animation={!single} />
				</div>
				<div className={styles.code}>{code}</div>
			</div>
			<span className={styles.close} onClick={togglePreviewModal}>
				x
			</span>
		</div>
	);
};

const mapStateToProps = state => ({
	params: state.project.params
});
const mapDispatchToProps = { togglePreviewModal };

export default connect(mapStateToProps, mapDispatchToProps)(PreviewModal);
