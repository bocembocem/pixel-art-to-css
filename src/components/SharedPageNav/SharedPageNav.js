import React from 'react';
import { NavLink } from 'react-router-dom';
import cn from 'classnames';

import styles from './SharedPageNav.module.css';;

export default ({ id, pagesCount }) => {  
  if (!pagesCount) {
    return null;
  }

  const buttons = [];
  let start = id - 3;
  let end = id + 3;

  if (start < 1) {
    start = 1;
    end += 3 + start;
  }

  if (end > pagesCount) {
    end = pagesCount;
  }
  
  for (let i = start; i <= end; i++) {
    const btn = (
      <NavLink to={`/shared/${i}`} key={i}>
        <span
          className={i === id
            ? cn(styles.button, styles.active)
            : styles.button}
        >
          {i}
        </span>
      </NavLink>
    );

    buttons.push(btn);
  }

  return (
    <div className={styles.nav}>
      {buttons}
    </div>
  );
};