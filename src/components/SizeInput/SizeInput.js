import React from 'react';

import styles from './SizeInput.module.css';

export default ({ title, value, changeHandler }) => (
	<div className={styles.wrapper}>
		<div className={styles.title}>{title}</div>
		<input
			type="text"
			className={styles.input}
			value={value}
			onChange={e => changeHandler(parseInt(e.target.value) || 1)}
		/>
	</div>
);
