import React, { useEffect, useReducer } from 'react';
import { connect } from 'react-redux';

import styles from './ColorPicker.module.css';

import HueSaturationScale from '../HueSaturationScale/HueSaturationScale';
import LightnessScale from '../LightnessScale/LightnessScale';
import AlphaScale from '../AlphaScale/AlphaScale';
import { setNewColorListItem } from '../../redux/reducers/colors/actions';
import { getRGB, getHSL } from '../../helpers/transformColor';

const types = {
  REFRESH_STATE: 'REFRESH_STATE',
  ON_MOVE: 'ON_MOVE',
  SET_SCALE_HS: 'SET_SCALE_HS',
  SET_SCALE_L: 'SET_SCALE_L',
  SET_SCALE_A: 'SET_SCALE_A'
};
const getState = color => {
  const [R, G, B, A] = color;
  const [H, S, L] = getHSL(R, G, B);

  return {
    move: false,
    scaleHS: { H, S },
    scaleL: L,
    scaleLColor: getRGB(H, S, 0.5),
    scaleA: A,
    scaleAColor: color,
    previewColor: color
  };
};
const reducer = (state, action) => {  
  const { H, S, L, A, scaleLColor, scaleAColor } = action.payload;

  switch (action.type) {
    case types.REFRESH_STATE:
      return action.payload;

    case types.ON_MOVE:
      return {
        ...state,
        move: action.payload
      };

    case types.SET_SCALE_HS:
      return {
        ...state,
        scaleHS: { H, S },
        scaleLColor,
        scaleAColor,
        previewColor: [...scaleAColor, state.scaleA]
      };

    case types.SET_SCALE_L:
      return {
        ...state,
        scaleL: L,
        scaleAColor,
        previewColor: [...scaleAColor, state.scaleA]
      };

    case types.SET_SCALE_A:
      return {
        ...state,
        scaleA: A,
        previewColor: [...state.scaleAColor.slice(0, 3), A]
      };

    default:
      return state;
  }
};

const NewColorPicker = ({ activeColorValue, setNewColorListItem }) => {
  const [state, dispatch] = useReducer(reducer, getState(activeColorValue));

  const setMove = move => dispatch({
    type: types.ON_MOVE,
    payload: move
  });
  const setScaleHS = ({ H, S }) => dispatch({
    type: types.SET_SCALE_HS,
    payload: {
      H,
      S,
      scaleLColor: getRGB(H, S, 0.5),
      scaleAColor: getRGB(H, S, state.scaleL)
    }
  });
  const setScaleL = L => dispatch({
    type: types.SET_SCALE_L,
    payload: {
      L,      
      scaleAColor: getRGB(state.scaleHS.H, state.scaleHS.S, L)
    }
  });
  const setScaleA = A => dispatch({
    type: types.SET_SCALE_A,
    payload: { A }
  });
  const refreshState = state => dispatch({
    type: types.REFRESH_STATE,
    payload: state
  });

  useEffect(() => {
    !state.move && setNewColorListItem(state.previewColor);
  }, [state.move]);
  useEffect(() => {
    if (state.previewColor.join('') !== activeColorValue.join('')) {
      refreshState(getState(activeColorValue));
    }     
  }, [activeColorValue]);

  const [R, G, B, A] = state.previewColor;

  return (
    <div className={styles.picker}>
      <div className={styles.top}>
        <HueSaturationScale
          scaleHS={state.scaleHS}
          setScaleHS={setScaleHS}
          move={state.move}
          setMove={setMove}
        />
        <AlphaScale
          scaleA={state.scaleA}
          setScaleA={setScaleA}
          move={state.move}
          setMove={setMove}
          scaleAColor={state.scaleAColor}
        />
      </div>
			<div className={styles.bottom}>
        <LightnessScale
          scaleL={state.scaleL}
          setScaleL={setScaleL}
          move={state.move}
          setMove={setMove}
          scaleLColor={state.scaleLColor}
        />
        <div
          className={styles.preview}
          style={{ backgroundColor: `rgba(${R},${G},${B},${A})` }}
        />
      </div>			
		</div>
  );
};

const mapStateToProps = state => ({
  activeColorValue: state.colors.colorList[state.colors.activeColorIdx]
});
const mapDispatchToProps = { setNewColorListItem };

export default connect(mapStateToProps,	mapDispatchToProps)(NewColorPicker);