import React, { useEffect,  useState } from 'react';
import { connect } from 'react-redux';

import styles from './Ground.module.css';

import Canvas from '../Canvas/Canvas';
import ListenerLayer from '../ListenerLayer/ListenerLayer';
import { setLayerColorMap } from '../../redux/reducers/project/actions';
import { setNewColorListItem } from '../../redux/reducers/colors/actions';

const getColor = (y, x, map) => map[y] && map[y][x] ? map[y][x].join('') : undefined;
const isEqualMaps = (first, second) => JSON.stringify(first) === JSON.stringify(second);
const onBrush = ({ y, x, map, activeColorValue }) => {
	const newMap = { ...map };

	newMap[y] = { ...newMap[y], [x]: activeColorValue };

	return newMap;
};
const onEraser = (y, x, map) => {
	const newMap = { ...map };
	const newY = { ...newMap[y] };

	delete newY[x];
	newMap[y] = newY;

	return newMap;
};
const onFillDrip = args => {
	const { y, x, map, gridX, gridY, currentColor, activeColorValue } = args;
	let newMap = { ...map };

	newMap[y] = { ...newMap[y], [x]: activeColorValue };

	if (y + 1 < gridY && getColor(y + 1, x, newMap) === currentColor) {
		newMap = onFillDrip({ ...args, y: y + 1, map: newMap });
	}

	if (y - 1 >= 0 && getColor(y - 1, x, newMap) === currentColor) {
		newMap = onFillDrip({ ...args, y: y - 1, map: newMap });
	}
	
	if (x + 1 < gridX && getColor(y, x + 1, newMap) === currentColor) {
		newMap = onFillDrip({ ...args, x: x + 1, map: newMap });
	}

	if (x - 1 >= 0 && getColor(y, x - 1, newMap) === currentColor) {
		newMap = onFillDrip({ ...args, x: x - 1, map: newMap });
	}

	return newMap;
};

const Ground = props => {
	const {
		layerMap,
		gridX,
		gridY,
		activeColorValue,
		setLayerColorMap,
		setNewColorListItem
	} = props;	
	const { eraser, brush, fillDrip, eyeDropper } = props.tools;

	const [move, setMove] = useState(false);
	const [map, setMap] = useState(layerMap);

	useEffect(() => {
		!isEqualMaps(map, layerMap) && setMap(layerMap);
	}, [layerMap]);

	const onStart = e => {
		setMove(true);
		updateColorMap(e);
	};
	const onMove = e => move && updateColorMap(e);
	const onEnd = () => {
		if ((eraser || brush || fillDrip) && !isEqualMaps(map, layerMap)) {
			setLayerColorMap(map);
		}

		setMove(false);
	};
	const updateColorMap = e => {
		const y = Math.floor(e.nativeEvent.layerY / ((e.target.offsetWidth) / gridX));
		const x = Math.floor(e.nativeEvent.layerX / ((e.target.offsetWidth) / gridX));		
		const currentColor = getColor(y, x, map);
		const equal = currentColor === activeColorValue.join('');

		brush && !equal && setMap(onBrush({ y, x, map, activeColorValue }));
		eraser && currentColor && setMap(onEraser(y, x, map));
		fillDrip && !equal && setMap(onFillDrip({ y, x, map, gridX, gridY, currentColor, activeColorValue }));
		eyeDropper && currentColor && setNewColorListItem(map[y][x]);
	};

	return (
		<div className={styles.ground}>
			<Canvas gridX={gridX} gridY={gridY} map={map} />
			<ListenerLayer onStart={onStart} onMove={onMove} onEnd={onEnd} />
		</div>
	);
};

const mapStateToProps = state => {
	const { activeLayerIdx, layerList, gridX, gridY } = state.project.params;
	const { activeColorIdx, colorList } = state.colors;

	return {
		layerMap: layerList[activeLayerIdx],
		gridX,
		gridY,
		activeColorValue: colorList[activeColorIdx],
		tools: state.tools
	};
};
const mapDispatchToProps = { setLayerColorMap, setNewColorListItem };

export default connect(mapStateToProps,	mapDispatchToProps)(Ground);