import React from 'react';
import { connect } from 'react-redux';

import styles from './Layers.module.css';

import Button from '../Button/Button';
import Layer from '../Layer/Layer';
import {
	addLayer,
	copyLayer,
	deleteLayer,
	changeActiveLayerIdx
} from '../../redux/reducers/project/actions';

const Layers = props => {
	const {
		activeLayerIdx,
		layerList,
		addLayer,
		copyLayer,
		deleteLayer,
		changeActiveLayerIdx
	} = props;

	const layers = layerList.map((layer, idx) => (
		<div className={styles.layer} key={idx}>
			<Layer
				layer={layer}
				activeLayer={activeLayerIdx === idx}
				position={parseInt((100 / layerList.length) * (idx + 1))}
				changeActiveLayerIdx={() => changeActiveLayerIdx(idx)}
				copyLayer={() => copyLayer(idx)}
				deleteLayer={() => deleteLayer(idx)}
			/>
		</div>
	));

	return (
		<div className={styles.layers}>
			<div className={styles.button}>
				<Button value={'+'} clickHandler={addLayer} />
			</div>
			{layers}
		</div>
	);
};

const mapStateToProps = state => ({
	activeLayerIdx: state.project.params.activeLayerIdx,
	layerList: state.project.params.layerList
});
const mapDispatchToProps = {
	addLayer,
	copyLayer,
	deleteLayer,
	changeActiveLayerIdx
};

export default connect(mapStateToProps, mapDispatchToProps)(Layers);
