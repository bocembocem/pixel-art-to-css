import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './Header.module.css';

import HeaderNav from '../HeaderNav/HeaderNav';

export default () => (
	<div className={styles.header}>
		<NavLink to='/'>
			<h1 className={styles.title}>PIXEL ART TO CSS</h1>
		</NavLink>
		<HeaderNav />
	</div>
);