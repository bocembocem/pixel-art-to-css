import React from 'react';

import styles from './Button.module.css';

export default ({ value, clickHandler }) => (
	<div className={styles.button} onClick={clickHandler}>
		{value}
	</div>
);
