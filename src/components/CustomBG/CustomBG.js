import React from 'react';

import styles from './CustomBG.module.css';

import { getBoxShadow } from '../../helpers/getBoxShadow';

export default ({ map, pixelSize }) => (
  <div className={styles.bg}>
    {getBoxShadow(map, pixelSize)}
  </div>
);