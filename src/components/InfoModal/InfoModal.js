import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import styles from './InfoModal.module.css';

import { toggleInfoModal } from '../../redux/reducers/modals/actions';

const InfoModal = ({ info, toggleInfoModal }) => {
  useEffect(() => {
    if (info.timeout) {
      const timeout = setTimeout(() => toggleInfoModal(false), 1500);

      return () => clearTimeout(timeout);
    }    
  }, [info.timeout]);

  return (
    <div className={styles.info}>
      <div className={styles.inner}>
        {info.value}
      </div>      
    </div>
  );
};

const mapStateToProps = state => ({ info: state.modals.info });
const mapDispatchToProps = { toggleInfoModal };

export default connect(mapStateToProps, mapDispatchToProps)(InfoModal);