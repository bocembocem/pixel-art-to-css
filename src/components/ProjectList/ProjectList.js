import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';

import styles from './ProjectList.module.css';

import ProjectPreview from '../ProjectPreview/ProjectPreview';

export default ({ projectList, deleteHandler, editorHandler, shareHandler }) => {
  const getButtons = idx => {
    if (!deleteHandler || !shareHandler) {
      return null;
    }

    return (
      <Fragment>
        <span className={styles.delete} onClick={() => deleteHandler(idx)}>x</span>
        <span className={styles.share} onClick={() => shareHandler(idx)}>share</span>
      </Fragment>
    );
  };
  const getAutor = (autor, id) => {
    if (!autor) {
      return null;
    }

    return (
      <div className={styles.autor}>
        <NavLink to={`/user/${id}`}>
          Autor: {autor}
        </NavLink>
      </div> 
    )
  };
  const getProjectParams = obj => obj.project || obj;
  const getProjectList = () => {
    if (!projectList) {
      return null;
    }

    if (!projectList.length) {
      return <h3>Project collection is epmty!</h3>;
    }

    return projectList.map((project, idx) =>
      <div className={styles.projectWrapper} key={idx}>
        <div className={styles.project} key={idx}>
          <ProjectPreview {...getProjectParams(project)} pixelSize={5} animation={true} />        
          <NavLink to='/'>
            <span className={styles.editor} onClick={() => editorHandler(idx)}>editor</span>
          </NavLink>              
          {getButtons(idx)}
        </div>
        {getAutor(project.name, project.id)}
      </div>
    );
  };

  return (
    <div className={styles.projectList}>
      {getProjectList()}
    </div>
  );
};