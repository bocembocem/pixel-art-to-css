import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import cn from 'classnames';

import styles from './LoadModal.module.css';

import ProjectPreview from '../ProjectPreview/ProjectPreview';
import {
  loadProject,
  changeProjectIdx
} from '../../redux/reducers/project/actions';
import {
  toggleAuthModal,
  toggleLoadModal,
  toggleInfoModal
} from '../../redux/reducers/modals/actions';
import apiWithInfoModal from '../../services/apiWithInfoModal';

const LoadModal = props => {
  const {
    token,
    activeProjectIdx,
    loadProject,
    changeProjectIdx,
    toggleAuthModal,
    toggleLoadModal,
    toggleInfoModal
  } = props;
  const [message, setMessage] = useState(null);
  const [projectList, setProjectList] = useState(null);

  const getProjectList = () => {
    if (!projectList || !projectList.length) {
      return null;
    }

    return projectList.map((project, idx) =>
      <div
        className={idx === activeProjectIdx
          ? cn(styles.project, styles.projectActive)
          : styles.project}
        onClick={e => e.nativeEvent.toElement.tagName !== 'SPAN'
          && loadProject(idx, project)}
        key={idx}
      >
        <ProjectPreview {...project} pixelSize={5} animation={true} />        
        <span className={styles.delete} onClick={() => deleteProject(idx)}>
          x
        </span>
      </div>
    );
  };
  const deleteProject = idx => {
    apiWithInfoModal.deleteProject(token, idx)
      .then(r => {
        if (r.status) {
          setProjectList(null);
          setProjectList(r.projects);

          if (activeProjectIdx) {
            if (idx === activeProjectIdx) {
              changeProjectIdx(null);
            } else {
              changeProjectIdx(activeProjectIdx - 1);
            }
          }
        }
      });
  };

  useEffect(() => {
    if (token) {
      apiWithInfoModal.loadProjects(token)
        .then(r => {
          if (r.status) {
            if (r.projects.length !== 0) {
              setProjectList(r.projects);
              setMessage('Select one of your awesome drawings!');
            } else {
              setMessage('You project collection is epmty!');
            }
          }
        });
    } else {
      toggleLoadModal();
      toggleInfoModal(true, 'Need auth', true);
      toggleAuthModal(true, true);
    }
  }, []);

  return (
    <div className={styles.load}>
      {message ? <h3 className={styles.title}>{message}</h3> : null}
      <div className={styles.projectList}>{getProjectList()}</div>
      <span className={styles.close} onClick={() => toggleLoadModal()}>
				x
			</span>
    </div>
  );
};

const mapStateToProps = state => ({
  token: state.user.token,
  activeProjectIdx: state.project.projectIdx
});
const mapDispatchToProps = {
  loadProject,
  changeProjectIdx,
  toggleAuthModal,
  toggleLoadModal,
  toggleInfoModal
};

export default connect(mapStateToProps, mapDispatchToProps)(LoadModal);