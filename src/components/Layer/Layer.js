import React from 'react';
import cn from 'classnames';

import styles from './Layer.module.css';

import { getBoxShadow } from '../../helpers/getBoxShadow';

export default props => {
	const {
		layer,
		activeLayer,
		position,
		changeActiveLayerIdx,
		copyLayer,
		deleteLayer
	} = props;

	return (
		<div
			className={activeLayer ? cn(styles.layer, styles.active) : styles.layer}
			onClick={e =>
				e.nativeEvent.toElement.tagName !== 'SPAN' && changeActiveLayerIdx()
			}
		>
			<div className={styles.top}>
				<span
					className={styles.preview}
					style={{ boxShadow: getBoxShadow(layer, 2) }}
				/>
				<span className={styles.delete} onClick={deleteLayer}>
					x
				</span>
				<span className={styles.copy} onClick={copyLayer}>
					copy
				</span>
			</div>
			<div className={styles.bottom}>{position}</div>
		</div>
	);
};
