import React from 'react';

import styles from './CanvasLine.module.css';

import CanvasLineItem from '../CanvasLineItem/CanvasLineItem';

export default ({ gridX, mapY }) => {
  const items = [];

	for (let x = 0; x < gridX; x++) {    
    const color = mapY ? mapY[x] || null : null;
    
		items[x] = <CanvasLineItem color={color} gridX={gridX} key={x} />;
	}

	return <div className={styles.line}>{items}</div>;
};