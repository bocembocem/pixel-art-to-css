import React from 'react';

import styles from './AlphaScale.module.css';

import ListenerLayer from '../ListenerLayer/ListenerLayer';

const _scaleHeight = 200;

export default ({ scaleA, setScaleA, move, setMove, scaleAColor }) => {
  const [R, G, B] = scaleAColor;
  const backgroundImage =
    `linear-gradient(to top, rgb(${R}, ${G}, ${B}), transparent)`;
  const top = `${scaleA * _scaleHeight - 5}px`;

  const onStart = e => {
    setMove(true);
    scaleChange(e);
  };
  const onMove = e => move && scaleChange(e);
  const onEnd = () => setMove(false);  
  const scaleChange = e => setScaleA(e.nativeEvent.layerY / _scaleHeight);

  return (
    <div className={styles.scale}>
      <div className={styles.alpha} />
      <div className={styles.layer} style={{ backgroundImage }} />
      <div className={styles.setter} style={{ top }} />
      <ListenerLayer onStart={onStart} onMove={onMove} onEnd={onEnd} />
    </div>
  );
};