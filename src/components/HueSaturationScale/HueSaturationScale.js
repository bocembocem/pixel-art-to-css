import React, { useEffect, useRef } from 'react';

import styles from './HueSaturationScale.module.css';

import ListenerLayer from '../ListenerLayer/ListenerLayer';

const _palette = [
	[255, 0, 0],
	[255, 255, 0],
	[0, 255, 0],
	[0, 255, 255],
	[0, 0, 255],
	[255, 0, 255],
	[255, 0, 0],
];
const _scaleWidth = 200;
const _scaleHeight = 200;

const drawPalette = paletteRef => {
	const context = paletteRef.current.getContext('2d');
	const gradient = context.createLinearGradient(0, 0, _scaleWidth, 0);

	for (let i = 0; i < _palette.length; i++) {
		const [R, G, B] = _palette[i];

		gradient.addColorStop(i / (_palette.length - 1), `rgb(${R},${G},${B})`);
	}

	context.fillStyle = gradient;
	context.fillRect(0, 0, _scaleWidth, _scaleHeight);
};

export default ({ scaleHS, setScaleHS, move, setMove }) => {
	const canvas = useRef(null);
	const scaleChange = e => {
		const x = e.nativeEvent.layerX / _scaleWidth;
		const y = 1 - e.nativeEvent.layerY / _scaleHeight;

		setScaleHS({ H: x, S: y });
	};
	const top = _scaleHeight - scaleHS.S * _scaleHeight - 5;
	const left = scaleHS.H * _scaleWidth - 5;

	useEffect(() => {
		drawPalette(canvas);
	}, []);

	const onStart = e => {
    setMove(true);
    scaleChange(e);
  };
  const onMove = e => move && scaleChange(e);
  const onEnd = () => setMove(false);

	return (
		<div className={styles.scale}>
			<canvas ref={canvas} width={_scaleWidth} height={_scaleHeight} />
			<div className={styles.layer} />
			<div className={styles.setter} style={{ top, left }} />
      <ListenerLayer onStart={onStart} onMove={onMove} onEnd={onEnd} />
		</div>
	);
};