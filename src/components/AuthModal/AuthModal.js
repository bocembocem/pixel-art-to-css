import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import styles from './AuthModal.module.css';

import TogglerButton from '../TogglerButton/TogglerButton';
import { onSignIn, onSignUp } from '../../redux/reducers/user/actions';
import { toggleAuthModal } from '../../redux/reducers/modals/actions';
import apiWithInfo from '../../services/apiWithInfoModal';

const AuthModal = ({ signUpTab, onSignIn, onSignUp, toggleAuthModal }) => {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [message, setMessage] = useState(null);

	const getNameInput = () => {
		if (!signUpTab) {
			return null;
		}

		return (
			<input
				type="text"
				placeholder="name"
				value={name}
				onChange={e => setName(e.target.value)}
			/>
		);
	};
	const submitHandler = async e => {
		e.preventDefault();

		let response = null;
		let callback = null;

		if (signUpTab) {
			response = await apiWithInfo.signUp(name, email, password);
			callback = () => onSignUp(response.token, response.name, response.id);
		} else {
			response = await apiWithInfo.signIn(email, password);
			callback = () => onSignIn(response.token, response.name, response.id);
		}

		if (response.status) {
			callback();
			toggleAuthModal(false);
		} else {
			setMessage(response.message);
		}
	};

	useEffect(() => {
		setMessage(null);
	}, [signUpTab]);

	return (
		<div className={styles.auth}>
			<div className={styles.inner}>
				<div className={styles.togglers}>
					<div className={styles.toggler}>
						<TogglerButton
							value={'Sign up'}
							clickHanler={() => toggleAuthModal(true, true)}
							active={signUpTab}
						/>
					</div>
					<div className={styles.toggler}>
						<TogglerButton
							value={'Sign in'}
							clickHanler={() => toggleAuthModal(true, false)}
							active={!signUpTab}
						/>
					</div>					
				</div>
				<div className={styles.message}>{message}</div>
				<form className={styles.form} onSubmit={submitHandler}>					
					<input
						type="text"
						placeholder="email"
						onChange={e => setEmail(e.target.value)}
						autoFocus
					/>
					{getNameInput()}
					<input
						type="password"
						placeholder="password"
						onChange={e => setPassword(e.target.value)}
					/>
					<button type="submit">
						{signUpTab ? 'Sign up' : 'Sign in'}
					</button>
				</form>
				<span
					className={styles.close}
					onClick={() => toggleAuthModal(false, true)}
				>
					x
				</span>
			</div>			
		</div>
	);
};

const mapStateToProps = state => ({ signUpTab: state.modals.auth.signUp });
const mapDispatchToProps = { onSignIn, onSignUp, toggleAuthModal };

export default connect(mapStateToProps, mapDispatchToProps)(AuthModal);
