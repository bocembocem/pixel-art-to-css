import React from 'react';

import styles from './LightnessScale.module.css';

import ListenerLayer from '../ListenerLayer/ListenerLayer';

const _scaleWidth = 200;

export default ({ scaleL, setScaleL, move, setMove, scaleLColor }) => {
  const [R, G, B] = scaleLColor;
  const scaleChange = e => setScaleL(e.nativeEvent.layerX / _scaleWidth);
  const backgroundImage =
    `linear-gradient(to right, rgb(0, 0, 0), rgb(${R}, ${G}, ${B}), rgb(255, 255, 255))`;
  const left = scaleL * _scaleWidth - 5;

  const onStart = e => {
    setMove(true);
    scaleChange(e);
  };
  const onMove = e => move && scaleChange(e);
  const onEnd = () => setMove(false);

  return (
    <div className={styles.scale}>
      <div className={styles.layer} style={{ backgroundImage }}	/>
      <div className={styles.setter} style={{ left }} />      
      <ListenerLayer onStart={onStart} onMove={onMove} onEnd={onEnd} />
    </div>
  );
};