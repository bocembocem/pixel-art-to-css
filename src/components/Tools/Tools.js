import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faEraser,
	faEyeDropper,
	faPaintBrush,
	faPalette,
	faFillDrip
} from '@fortawesome/free-solid-svg-icons';

import styles from './Tools.module.css';

import {
	changeEraser,
	changeEyeDropper,
	changeFillDrip,
	changeBrush,
	toggleColorPicker
} from '../../redux/reducers/tools/actions';

const Tools = props => {
	const {
		eraser,
		eyeDropper,
		fillDrip,
		brush,
		colorPicker,
		changeEraser,
		changeEyeDropper,
		changeFillDrip,
		changeBrush,
		toggleColorPicker
	} = props;

	return (
		<div className={styles.tools}>
			<FontAwesomeIcon
				className={eraser ? styles.activeTool : styles.tool}
				size="2x"
				icon={faEraser}
				onClick={changeEraser}
			/>
			<FontAwesomeIcon
				className={eyeDropper ? styles.activeTool : styles.tool}
				size="2x"
				icon={faEyeDropper}
				onClick={changeEyeDropper}
			/>
			<FontAwesomeIcon
				className={fillDrip ? styles.activeTool : styles.tool}
				size="2x"
				icon={faFillDrip}
				onClick={changeFillDrip}
			/>
			<FontAwesomeIcon
				className={brush ? styles.activeTool : styles.tool}
				size="2x"
				icon={faPaintBrush}
				onClick={changeBrush}
			/>
			<FontAwesomeIcon
				className={colorPicker ? styles.activeTool : styles.tool}
				size="2x"
				icon={faPalette}
				onClick={toggleColorPicker}
			/>
		</div>
	);
};

const mapStateToProps = state => ({ ...state.tools });
const mapDispatchToProps = {
	changeEraser,
	changeEyeDropper,
	changeFillDrip,
	changeBrush,
	toggleColorPicker
};

export default connect(mapStateToProps,	mapDispatchToProps)(Tools);