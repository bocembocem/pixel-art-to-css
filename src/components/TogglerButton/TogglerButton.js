import React from 'react';

import styles from './TogglerButton.module.css';

export default ({ value, clickHanler, active }) => (
  <div
    className={active ? styles.togglerActive : styles.toggler}
    onClick={clickHanler}
  >
    {value}
  </div>
);