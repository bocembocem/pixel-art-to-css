import firebase from '../config/firebase';

const getCollectionDocument = async (collection, doc) =>
	await firebase.firestore()
		.collection(collection).doc(doc.toString()).get()
			.then(r => r.data());

const setCollectionDocument = async (collection, doc, data) =>
	await firebase.firestore()
		.collection(collection).doc(doc.toString()).set(data);

export const signIn = async (email, password) => {
	const result = {};

	try {
		let response = await firebase.auth().signInWithEmailAndPassword(email, password);

		result.token = response.user.uid;
		response = await getCollectionDocument('users', result.token);
		result.id = response.id;
		result.name = response.name;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const signUp = async (name, email, password) => {
	const result = {};

	try {
		let response = await firebase.auth().createUserWithEmailAndPassword(email, password);

		result.token = response.user.uid;
		response = await getCollectionDocument('id', 'index');
		response[response.current] = result.token;
		result.id = response.current;
		response.current += 1;

		await setCollectionDocument('id', 'index', response);
		await setCollectionDocument('users', result.token, { name, id: result.id });
		await setCollectionDocument('projects', result.token, { projects: [] });
		await setCollectionDocument('shared projects', result.token, { projects: [] });
		
		result.name = name;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const signOut = async () => {
	const result = {};

	try {
		await firebase.auth().signOut();
		
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const loadProjects = async token => {
	const result = {};

	try {
		const data = await getCollectionDocument('projects', token);

		result.projects = data.projects;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const loadUserSharedProjects = async token => {
	const result = {};

	try {
		const data = await getCollectionDocument('shared projects', token);

		result.projects = data.projects;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const deleteProject = async (token, idx) => {
	const result = {};

	try {
		const data = await getCollectionDocument('projects', token);

		data.projects.splice(idx, 1);
		await setCollectionDocument('projects', token, data);

		result.projects = data.projects;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const saveProject = async (token, project, idx) => {
	const result = {};

	try {
		const data = await getCollectionDocument('projects', token);

		if (idx === null) {
			data.projects.push(project);
		} else {
			data.projects[idx] = project;
		}

		await setCollectionDocument('projects', token, data);

		result.idx = idx || data.projects.length - 1;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const shareProject = async ({ token, name, id, project }) => {
	const result = {};

	try {
		let { current } = await getCollectionDocument('all shared projects', 'info');
		let data = await getCollectionDocument('all shared projects', current);

		data.projects.push({ name, id, project });
		await setCollectionDocument('all shared projects', current, data);

		if (data.projects.length >= 10) {
			current += 1;
			await setCollectionDocument('all shared projects', current, { projects: [] });
			await setCollectionDocument('all shared projects', 'info', { current });
		}

		data = await getCollectionDocument('shared projects', token);
		data.projects.push(project);		
		await setCollectionDocument('shared projects', token, data);

		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const loadSharedProjects = async id => {
	const result = {};

	try {
		const data = await getCollectionDocument('all shared projects', id);

		result.projects = data.projects;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const getSharedProjectsCount = async () => {
	const result = {};

	try {
		const data = await getCollectionDocument('all shared projects', 'info');

		result.count = data.current;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const getUserInfo = async id => {
	const result = {};

	try {
		let response = await getCollectionDocument('id', 'index');

		result.token = response[id];
		response = await getCollectionDocument('users', result.token);
		result.name = response.name;
		response = await loadUserSharedProjects(result.token);
		result.projects = response.projects;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};

export const getUserCount = async () => {
	const result = {};

	try {
		const response = await getCollectionDocument('id', 'index');

		result.current = response.current;
		result.status = true;
	} catch (error) {
		result.message = error.message;
	}

	return result;
};