import store from '../redux/store/store';
import * as api from './api';
import { toggleInfoModal } from '../redux/reducers/modals/actions';

const apiWithInfoModal = {};

for (let f in api) {
  apiWithInfoModal[f] = async (...args) => {
    store.dispatch(toggleInfoModal(true, 'Waiting...'));

    const result = await api[f](...args);

    if (result.status) {
      store.dispatch(toggleInfoModal(true, 'Complete!', true));      
    } else {
      store.dispatch(toggleInfoModal(true, result.message, true));
    }    
    
    return result;
  }
}

export default apiWithInfoModal;