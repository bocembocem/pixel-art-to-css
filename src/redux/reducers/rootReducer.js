import { combineReducers } from 'redux';

import colors from './colors/reducer';
import modals from './modals/reducer';
import project from './project/reducer';
import tools from './tools/reducer';
import user from './user/reducer';

export default combineReducers({
  colors,
  modals,
  project,
  tools,
  user
});