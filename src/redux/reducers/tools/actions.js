import {
  CHANGE_ERASER,
  CHANGE_EYE_DROPPER,
  CHANGE_FILL_DRIP,
  CHANGE_BRUSH,
  TOGGLE_COLOR_PICKER
} from './types';

export const changeEraser = () => ({
	type: CHANGE_ERASER
});

export const changeEyeDropper = () => ({
	type: CHANGE_EYE_DROPPER
});

export const changeFillDrip = () => ({
  type: CHANGE_FILL_DRIP
});

export const changeBrush = () => ({
	type: CHANGE_BRUSH
});

export const toggleColorPicker = () => ({
	type: TOGGLE_COLOR_PICKER
});