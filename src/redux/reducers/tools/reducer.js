import {
	CHANGE_ERASER,
	CHANGE_EYE_DROPPER,
	CHANGE_FILL_DRIP,
	CHANGE_BRUSH,
	TOGGLE_COLOR_PICKER
} from './types';

const initialState = {
	eraser: false,
	eyeDropper: false,
	fillDrip: false,
	brush: true,
	colorPicker: false
};

export default (state = initialState, action) => {
	switch (action.type) {
		case CHANGE_ERASER:
			return {
				eraser: true,
				eyeDropper: false,
				fillDrip: false,
				brush: false,
				colorPicker: false
			};

		case CHANGE_EYE_DROPPER:
			return {
				...state,
				eraser: false,
				eyeDropper: true,
				fillDrip: false,
				brush: false
			};

		case CHANGE_FILL_DRIP:
			return {
				...state,
				eraser: false,
				eyeDropper: false,
				fillDrip: true,
				brush: false
			}

		case CHANGE_BRUSH:
			return {
				...state,
				eraser: false,
				eyeDropper: false,
				fillDrip: false,
				brush: true
			};

		case TOGGLE_COLOR_PICKER:
			return {
				...state,
				colorPicker: !state.colorPicker
			};

		default:
			return state;
	}
};
