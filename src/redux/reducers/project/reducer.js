import {
	ADD_PROJECT,
	LOAD_PROJECT,
	CHANGE_PROJECT_IDX,
	ADD_LAYER,
	COPY_LAYER,
	DELETE_LAYER,
	CHANGE_ACTIVE_LAYER_IDX,
	SET_LAYER_COLOR_MAP,
	RESET_LAYER_COLOR_MAP,
	ON_HISTORY_PREV,
	ON_HISTORY_NEXT,
	CHANGE_GRID_SIZE,
	CHANGE_PIXEL_SIZE,
	CHANGE_DURATION,
} from './types';

const NEW_PROJECT_TEMPLATE = {
	activeLayerIdx: 0,
	layerList: [{}],
	gridX: 16,
	gridY: 16,
	pixelSize: 5,
	duration: 1
};
const initialState = {
	projectIdx: null,
	activeHistoryIdx: 0,
	historyList: [NEW_PROJECT_TEMPLATE],
	params: NEW_PROJECT_TEMPLATE,
};

export default (state = initialState, action) => {
	const { activeHistoryIdx, historyList, params } = state;
	const { activeLayerIdx, layerList, gridX, gridY } = params;

	let changes = null;
	const historyChanges =
		activeHistoryIdx + 1 === historyList.length
			? [...historyList]
			: historyList.slice(0, activeHistoryIdx + 1);

	switch (action.type) {
		case ADD_PROJECT:
			return {
				projectIdx: null,
				activeHistoryIdx: 0,
				historyList: [NEW_PROJECT_TEMPLATE],
				params: NEW_PROJECT_TEMPLATE
			};

		case LOAD_PROJECT:
			return {
				projectIdx: action.payload.idx,
				activeHistoryIdx: 0,
				historyList: [action.payload.params],
				params: action.payload.params
			};

		case CHANGE_PROJECT_IDX:
			return {
				...state,
				projectIdx: action.payload
			};

		case ADD_LAYER:
			changes = {
				...params,
				activeLayerIdx: layerList.length,
				layerList: [...layerList, {}],
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case COPY_LAYER:
			changes = {
				...params,
				activeLayerIdx: action.payload + 1,
				layerList: [
					...layerList.slice(0, action.payload + 1),
					{ ...layerList[action.payload] },
					...layerList.slice(action.payload + 1, layerList.length)
				],
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case DELETE_LAYER:
			changes = {
				...params,
				activeLayerIdx:
					activeLayerIdx !== 0 && activeLayerIdx !== action.payload
						? activeLayerIdx - 1
						: 0,
				layerList:
					layerList.length > 1
						? layerList.filter((_, idx) => idx !== action.payload)
						: [{}]
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case CHANGE_ACTIVE_LAYER_IDX:
			changes = {
				...params,
				activeLayerIdx: action.payload
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case SET_LAYER_COLOR_MAP:
			changes = {
				...params,
				layerList: layerList.map((item, idx) =>
					idx === activeLayerIdx ? action.payload : item
				)
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case RESET_LAYER_COLOR_MAP:
			changes = {
				...params,
				layerList: layerList.map((item, idx) => idx === activeLayerIdx ? {} : item)
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case ON_HISTORY_PREV:
			return activeHistoryIdx !== 0
				? {
						...state,
						activeHistoryIdx: activeHistoryIdx - 1,
						params: { ...historyList[activeHistoryIdx - 1] }
				  }
				: state;

		case ON_HISTORY_NEXT:
			return historyList[activeHistoryIdx + 1]
				? {
						...state,
						activeHistoryIdx: activeHistoryIdx + 1,
						params: { ...historyList[activeHistoryIdx + 1] }
				  }
				: state;

		case CHANGE_GRID_SIZE:
			changes = {
				...params,
				gridX: action.payload.gridX || gridX,
				gridY: action.payload.gridY || gridY,
				layerList: layerList.map(map => {
					let newMap = { ...map };

					if (action.payload.gridY !== null) {
						for (let y in newMap) {
							if (y >= action.payload.gridY) {
								delete newMap[y];
							}
						}						
					} else {
						for (let y in newMap) {
							for (let x in newMap[y]) {
								if (x >= action.payload.gridX) {
									delete newMap[y][x];
								}
							}
							
						}
					}

					return newMap;
				})
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case CHANGE_PIXEL_SIZE:
			changes = {
				...params,
				pixelSize: action.payload
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		case CHANGE_DURATION:
			changes = {
				...params,
				duration: action.payload
			};

			return {
				...state,
				activeHistoryIdx: activeHistoryIdx + 1,
				historyList: [...historyChanges, changes],
				params: changes
			};

		default:
			return state;
	}
};
