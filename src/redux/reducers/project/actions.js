import {
	ADD_PROJECT,
	LOAD_PROJECT,
	CHANGE_PROJECT_IDX,
	ADD_LAYER,
	COPY_LAYER,
	DELETE_LAYER,
	CHANGE_ACTIVE_LAYER_IDX,
	SET_LAYER_COLOR_MAP,
	RESET_LAYER_COLOR_MAP,
	ON_HISTORY_PREV,
	ON_HISTORY_NEXT,
	CHANGE_GRID_SIZE,
	CHANGE_PIXEL_SIZE,
	CHANGE_DURATION,
} from './types';

export const addProject = () => ({
	type: ADD_PROJECT
});

export const loadProject = (idx, params) => ({
	type: LOAD_PROJECT,
	payload: { idx, params }
});

export const changeProjectIdx = idx => ({
	type: CHANGE_PROJECT_IDX,
	payload: idx
});

export const addLayer = () => ({
	type: ADD_LAYER
});

export const copyLayer = idx => ({
	type: COPY_LAYER,
	payload: idx
});

export const deleteLayer = idx => ({
	type: DELETE_LAYER,
	payload: idx
});

export const changeActiveLayerIdx = idx => ({
	type: CHANGE_ACTIVE_LAYER_IDX,
	payload: idx
});

export const setLayerColorMap = map => ({
	type: SET_LAYER_COLOR_MAP,
	payload: map
});

export const resetLayerColorMap = () => ({
	type: RESET_LAYER_COLOR_MAP,
});

export const onHistoryPrev = () => ({
	type: ON_HISTORY_PREV,
});

export const onHistoryNext = () => ({
	type: ON_HISTORY_NEXT,
});

export const changeGridSize = ({ value, gridX = null, gridY = null }) => ({
	type: CHANGE_GRID_SIZE,
	payload: { value, gridX, gridY }
});

export const changePixelSize = value => ({
	type: CHANGE_PIXEL_SIZE,
	payload: value
});

export const changeDuration = value => ({
	type: CHANGE_DURATION,
	payload: value
});