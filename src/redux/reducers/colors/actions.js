import {
	CHANGE_ACTIVE_COLOR_IDX,
	SET_NEW_COLOR_LIST_ITEM
} from './types';

export const changeActiveColorIdx = idx => ({
	type: CHANGE_ACTIVE_COLOR_IDX,
	payload: idx
});

export const setNewColorListItem = color => ({
	type: SET_NEW_COLOR_LIST_ITEM,
	payload: color
});