import {
	CHANGE_ACTIVE_COLOR_IDX,
	SET_NEW_COLOR_LIST_ITEM
} from './types';

const initialState = {
	activeColorIdx: 0,
	colorList: [
		[0, 0, 0, 1],
		[127, 127, 127, 1],
		[136, 0, 21, 1],
		[237, 28, 36, 1],
		[255, 127, 39, 1],
		[255, 242, 0, 1],
		[34, 177, 76, 1],
		[0, 162, 232, 1],
		[63, 72, 204, 1],
		[163, 73, 164, 1],
		[200, 191, 231, 1],
		[112, 146, 190, 1],
		[153, 217, 234, 1],
		[181, 230, 29, 1],
		[239, 228, 176, 1],
		[255, 201, 14, 1],
		[255, 174, 201, 1],
		[185, 122, 87, 1],
		[195, 195, 195, 1],
		[255, 255, 255, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1],
		[56, 56, 56, 1]
	],
};

export default (state = initialState, action) => {
	const { activeColorIdx, colorList } = state;

	switch (action.type) {
		case CHANGE_ACTIVE_COLOR_IDX:
			return {
				...state,
				activeColorIdx: action.payload,
			};

		case SET_NEW_COLOR_LIST_ITEM:
			return {
				...state,
				colorList: colorList.map((item, idx) => 
					idx === activeColorIdx ? action.payload : item)
			};

		default:
			return state;
	}
};
