import  {
  ON_SIGN_IN,
  ON_SIGN_OUT,
  ON_SIGN_UP
} from './types';

export const onSignIn = (token, name, id) => ({
	type: ON_SIGN_IN,
	payload: { token, name, id },
});

export const onSignOut = () => ({
	type: ON_SIGN_OUT,
});

export const onSignUp = (token, name, id) => ({
	type: ON_SIGN_UP,
	payload: { token, name, id }
});