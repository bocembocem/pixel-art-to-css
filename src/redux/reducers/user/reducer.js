import {
	ON_SIGN_IN,
	ON_SIGN_OUT,
	ON_SIGN_UP
} from './types';

const initialState = {
	token: null,
	name: null,
	id: null
};

export default (state = initialState, action) => {
	switch (action.type) {
		case ON_SIGN_IN:
			return {
				token: action.payload.token,
				name: action.payload.name,
				id: action.payload.id
			};

		case ON_SIGN_OUT:
			return {
				token: null,
				name: null,
			};

		case ON_SIGN_UP:
			return {
				token: action.payload.token,
				name: action.payload.name,
				id: action.payload.id
			};

		default:
			return state;
	}
};
