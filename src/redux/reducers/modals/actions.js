import {
  TOGGLE_AUTH_MODAL,
  TOGGLE_LOAD_MODAL,
  TOGGLE_PREVIEW_MODAL,
  TOGGLE_INFO_MODAL
} from './types';

export const toggleAuthModal = (visible, signUp) => ({
  type: TOGGLE_AUTH_MODAL,
  payload: { visible, signUp }
});

export const toggleLoadModal = () => ({
	type: TOGGLE_LOAD_MODAL
});

export const togglePreviewModal = () => ({
	type: TOGGLE_PREVIEW_MODAL
});

export const toggleInfoModal = (visible, value = null, timeout = false) => ({
  type: TOGGLE_INFO_MODAL,
  payload: { visible, value, timeout }
});