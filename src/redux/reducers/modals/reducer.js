import {
  TOGGLE_AUTH_MODAL,
  TOGGLE_LOAD_MODAL,
  TOGGLE_PREVIEW_MODAL,
  TOGGLE_INFO_MODAL
} from './types';

const initialState = {
	auth: {
    visible: false,
    signUp: true
  },
  load: false,
  preview: false,
  info: {
    visible: false,
    value: null,
    timeout: false
  }
};

export default (state = initialState, action) => {
	switch (action.type) {
		case TOGGLE_AUTH_MODAL:
			return {
        ...state,
        auth: {
          visible: action.payload.visible,
				  signUp: action.payload.signUp
        }				
			};

		case TOGGLE_LOAD_MODAL:
			return {
        ...state,
        load: !state.load
			};

		case TOGGLE_PREVIEW_MODAL:
			return {
				...state,
        preview: !state.preview
      };
      
    case TOGGLE_INFO_MODAL:
      return {
        ...state,
        info: {
          visible: action.payload.visible,
          value: action.payload.value,
          timeout: action.payload.timeout
        }
      };

		default:
			return state;
	}
};
