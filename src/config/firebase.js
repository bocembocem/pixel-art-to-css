import firebase from 'firebase';

const config = {
	apiKey: 'AIzaSyA1J2BrdsARHVONH2eoZdW-uev7klfqNiw',
	authDomain: 'pixel-art-to-css.firebaseapp.com',
	databaseURL: 'https://pixel-art-to-css.firebaseio.com',
	projectId: 'pixel-art-to-css',
	storageBucket: 'pixel-art-to-css.appspot.com',
	messagingSenderId: '718344286277',
	appId: '1:718344286277:web:16593b0be481650c62b32c'
};

firebase.initializeApp(config);

export default firebase;